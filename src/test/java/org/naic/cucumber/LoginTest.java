package org.naic.cucumber;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class LoginTest {
    private static WebDriver webDriver;
    private static String naicLoginPortal = "";
    private static String usernameId = "email";
    private static String passwordId = "pw-input";
    private static String loginSubmitButtonId = "sign-in";

    @Before
    public void setUp() throws Exception {
        WebDriverManager.chromedriver().setup();

        ChromeOptions option = new ChromeOptions();
        option.addArguments( "--headless" );
        option.addArguments( "--ignore-certificate-errors" );
        option.addArguments( "--no-sandbox" );
        option.addArguments( "--disable-extensions" );
        option.addArguments( "--disable-dev-shm-usage" );
        option.setCapability( ChromeOptions.CAPABILITY, option );

        webDriver = new ChromeDriver();
    }

    @After
    public void tearDown() throws Exception {
        webDriver.quit();
    }

    @Given("NAIC Login Portal {string}")
    public void naicLoginPortal( final String url ) {
        naicLoginPortal = url;
        webDriver.get( naicLoginPortal );

    }

    @When( "User entered valid username {string}" )
    public void validUsername( String username ) {
        webDriver.findElement( By.id( usernameId ) ).sendKeys( username );
        webDriver.findElement( By.id( loginSubmitButtonId ) ).submit();
    }
    @And("User entered invalid password {string}")
    public void invalidUserPassword (String password){
        webDriver.findElement( By.id( passwordId ) ).sendKeys( password );
        webDriver.findElement( By.id( loginSubmitButtonId ) ).submit();
    }


    @Then("Error message {string}")
    public void errorMessage(String expectedErrorMessage) {
        String errorField = "sign-up-title";
        String actualErrorMessage = webDriver.findElement(By.id(errorField)).getText();
        Assert.assertEquals(expectedErrorMessage, actualErrorMessage);
    }


    @Then("Email Error Message {string}")
    public void emailErrorMessage(String expectedErrorMessage) {
        String actualErrorMessage = webDriver.findElement(By.id("email-error")).getText();
        Assert.assertEquals(expectedErrorMessage,actualErrorMessage);
    }

    @Then("Password Error Message {string}")
    public void passwordErrorMessage(String expectedErrorMessage) {
        String actualErrorMessage = webDriver.findElement(By.id("pw-input-error")).getText();
        Assert.assertEquals(expectedErrorMessage, actualErrorMessage);
    }

    @Given("Navigate to website {string}")
    public void navigateToWebsite(String arg0) {

    }
}
