Feature: NAIC Invalid Login

  Scenario: valid username and password
    Given NAIC Login Portal "https://www.macys.com/account/signin"
    When User entered valid username "dfitsamtest@junk.com"
    And  User entered invalid password "IncorrectPassword"
    Then Error message "No account yet?"

  Scenario: valid username
    Given NAIC Login Portal "https://www.macys.com/account/signin"
    When User entered valid username "dfitsamtest@junk"
    And  User entered invalid password "IncorrectPassword"
    Then Email Error Message "Please enter your email address in this format: jane@company.com"

  Scenario: No password enter
    Given NAIC Login Portal "https://www.macys.com/account/signin"
    When User entered valid username "dfitsamtest@junk.com"
    And  User entered invalid password ""
    Then Password Error Message "Please enter your password"