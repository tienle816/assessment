Feature: dealership vehicle search

#  Scenario: Select Vehicle Make
#    Given Navigate to dealership "https://www.subaruofjanesville.com/used-inventory/index.htm"
#    Then Select Make menu
#    Then User select vehicle Make
#    And Verify vehicle search results

  Scenario: Search for vehicle using search bar
    Given Navigate to dealership "https://www.subaruofjanesville.com/used-inventory/index.htm"
    Then Input Vehicle Make In Search Bar "Toyota"
    And Customer click search button
    And Select preferred model