Feature: GoogleSearch

  Scenario: Search for vehicle using search bar
    Given Navigate to google "http://www.google.com"
    Then Enter search in search bar "Cute puppies"
    Then Press enter key
    Then Verify search results count

  Scenario: Time taken to search
    Given Navigate to google "http://www.google.com"
    Then Enter search in search bar "Cute puppies"
    Then Press enter key
    Then Verify time taken for search
#
#  Scenario: Google Sign-In
#    Given Navigate to google "http://www.google.com"


