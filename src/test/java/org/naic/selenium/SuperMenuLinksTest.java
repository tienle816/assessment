package org.naic.selenium;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.util.List;

public class SuperMenuLinksTest {
    private static WebDriver webDriver;
    private static String superMenu = "block-megamenu";
    private static String href = "href";
    private static String naicHome = "https://www.naic.org";
  //  private static String validDomain = "https://www.naic.org";
//    content.naic.org is the actual valid domain
    private static String validDomain = "https://content.naic.org";

    @Before
    public void setUp() throws Exception {
        WebDriverManager.chromedriver().setup();
        ChromeOptions option = new ChromeOptions();
        option.addArguments( "--headless" );
        option.addArguments( "--ignore-certificate-errors" );
        option.addArguments( "--no-sandbox" );
        option.addArguments( "--disable-extensions" );
        option.addArguments( "--disable-dev-shm-usage" );
        option.setCapability(ChromeOptions.CAPABILITY, option);

        webDriver = new ChromeDriver();
        webDriver.get( naicHome );
    }

    @After
    public void tearDown() throws Exception {
        webDriver.quit();
    }

    @Test
    public void testSuperMenuCountAndLinks() {
        WebElement superMenuElement = webDriver.findElement( By.id( superMenu ) );

        Assert.assertNotNull( superMenu + " element not found!", superMenuElement );
        List<WebElement> superMenuLinks = superMenuElement.findElements( By.xpath( "//*[@id=" + '"' + superMenu + "\"]/ul/li/a" ) );

        Assert.assertEquals( 7, superMenuLinks.size() );

        boolean validDomains = superMenuLinks.stream().allMatch( e -> e.getAttribute( href ).startsWith( validDomain ) );

        if ( !validDomains ) {
            superMenuLinks.stream().forEach( e -> System.out.println( e.getAttribute( href ) ) );
        }
        Assert.assertTrue( validDomains );
    }
}