package org.naic.cucumber;

import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GoogleSearch {

    private static WebDriver webDriver;
    private static String googleWebsite = "";

    @Before
    public void setUp() throws Exception {
        WebDriverManager.chromedriver().setup();

        ChromeOptions option = new ChromeOptions();
        option.addArguments("--headless");
        option.addArguments("--ignore-certificate-errors");
        option.addArguments("--no-sandbox");
        option.addArguments("--disable-extensions");
        option.addArguments("--disable-dev-shm-usage");
        option.setCapability(ChromeOptions.CAPABILITY, option);

        webDriver = new ChromeDriver();
    }

    @Given("Navigate to google {string}")
    public void setGoogleWebsite(final String url) {
        googleWebsite = url;
        webDriver.get(googleWebsite);


    }

    @Then("Enter search in search bar {string}")
    public void enterSearchInSearchBar(String arg0) {
        webDriver.findElement(By.xpath("//input[@type='text']")).sendKeys("Cute puppies");


    }


    @Then("Press enter key")
    public void userPressEnterKey() {
        webDriver.findElement(By.xpath("//input[@type='text']")).sendKeys(Keys.ENTER);


    }

    @Then("Verify search results count")
    public void verifySearchResultsCount() {
        String expectedResultsCounts = "About";
        String actualResultsCounts = webDriver.findElement(By.id("result-stats")).getText();
        Assert.assertTrue(expectedResultsCounts, actualResultsCounts.contains("About"));
    }

    @Then("Verify time taken for search")
    public void verifyTimeTakenForSearch() {
        String acceptableTimeTaken = "seconds";
        String actualTimeTaken = webDriver.findElement(By.xpath("//div[@id='result-stats']//nobr")).getText();
        Assert.assertTrue(actualTimeTaken, actualTimeTaken.contains("seconds"));
        System.out.println(actualTimeTaken);
        Assert.assertTrue(compareTimeTaken(1, Double.parseDouble(extractSubstring(actualTimeTaken))));


    }

    private boolean compareTimeTaken(double targetTime, double actualTime) {
        return actualTime <= targetTime ? true : false;

    }

    private String extractSubstring(String deleteSeconds) {
        System.out.println("in: " + deleteSeconds);
        StringBuffer results = new StringBuffer();

        String patternString1 = "([^( seconds)])";

        Pattern pattern = Pattern.compile(patternString1);
        Matcher matcher = pattern.matcher(deleteSeconds);

        while (matcher.find()) {
            results.append(matcher.group(1));
        }

        System.out.println("processed: " + results);
        return results.toString();



    }

}



