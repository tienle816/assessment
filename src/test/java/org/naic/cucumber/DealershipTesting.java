package org.naic.cucumber;


import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;


public class DealershipTesting {
    private static WebDriver webDriver;
    private static String dealersshipWebsite = "";

    @Before
    public void setUp() throws Exception {
        WebDriverManager.chromedriver().setup();

        ChromeOptions option = new ChromeOptions();
        option.addArguments("--headless");
        option.addArguments("--ignore-certificate-errors");
        option.addArguments("--no-sandbox");
        option.addArguments("--disable-extensions");
        option.addArguments("--disable-dev-shm-usage");
        option.setCapability(ChromeOptions.CAPABILITY, option);

        webDriver = new ChromeDriver();
    }

    //    @After
//    public void tearDown() throws Exception {
//        webDriver.quit();
    //}
    @Given("Navigate to dealership {string}")
    public void dealersshipWebsite(final String url) {
        dealersshipWebsite = url;
        webDriver.get(dealersshipWebsite);

    }
//
//    //Click Make menu
//    @Then("Select Make menu")
//    public void selectMakeMenu() {
//
//        WebElement Make = webDriver.findElement(By.xpath("//*[@id='make']"));
//        Make.click();
//    }
//
//    //Selecting vehicle make
//    @Then("User select vehicle Make")
//    public void userSelectVehicleMake() throws InterruptedException {
//        WebElement vehicleMake = webDriver.findElement(By.xpath("//label[@for='make-Subaru']"));
//        vehicleMake.click();
//
//    }
//
//    // Verify search results
//    @And("Verify vehicle search results")
//    public void verifyVehicleSearchResults() throws InterruptedException {
//        String expectedMessage = "Vehicles Matching";
//        String actualMessage = webDriver.findElement(By.xpath("//span[@class='d-none d-sm-inline']")).getText();
//        Assert.assertTrue(actualMessage.contains(expectedMessage));
//    }

    @Then("Input Vehicle Make In Search Bar {string}")
    public void inputVehicleMakeInSearchBar(String arg0) {
        webDriver.findElement(By.id("free-text-search-input")).sendKeys("Toyota");

    }

    @And("Customer click search button")
    public void customerClickSearchButton() {
        WebElement searchButton = webDriver.findElement(By.xpath("//button[@title='Search']"));
        searchButton.click();

    }

    @And("Select preferred model")
    public void selectPreferredModel() {
        WebElement firstResult = webDriver.findElement(By.xpath("//div[@data-index='0']/div/a"));
        firstResult.click();
//        WebElement model = webDriver.findElement(By.xpath("//div[@class='slick-slider carousel indicators-after slick-initialized']/div/div/div"));
//        model.click();
    }
}



